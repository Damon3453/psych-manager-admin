import VueRouter from 'vue-router'

// import Login from '../views/admin/login/Login.vue'
// import LoginAside from '../views/admin/login/Aside.vue'
// import LoginHeader from '../views/admin/login/Header.vue'
// import LoginFooter from '../views/admin/login/Footer.vue'

import NotFound from '../views/admin/layouts/NotFound.vue'
import Sidebar from '../views/admin/layouts/Sidebar.vue'
import Header from '../views/admin/layouts/Header.vue'
// import Footer from '../views/admin/layouts/Footer.vue'

import Main from '../views/admin/main/Main.vue'

const routes = [
  {
    path: '*',
    components: { default: NotFound }
  },
  {
    path: '/admin/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/admin/login/Login.vue'),
    // components: { sidebar: LoginAside, header: LoginHeader, default: Login, footer: LoginFooter },
  },
  {
    path: '/admin',
    // component: () => import('../views/admin/main/Main.vue'),
    components: { sidebar: Sidebar, header: Header, default: Main },
    children: []
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
