import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import VueRouter from 'vue-router'
import axios from 'axios'

import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
import '../public/element-theme/theme/index.css'

import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'

locale.use(lang)

Vue.use(VueRouter);
Vue.use(ElementUI);

Vue.config.productionTip = false

Vue.prototype.$http = axios.create({
  withCredentials: true,
  baseURL: process.env.VUE_APP_BASE_URL,
  headers: {
    Pragma: 'no-cache',
    'X-Api-Key': localStorage.getItem('access_token'),
  }
})

Vue.prototype.$localStorage = localStorage;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

